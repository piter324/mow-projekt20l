wine.data <- RoughSetData$wine.dt
set.seed(13)
wine.data <- wine.data[sample(nrow(wine.data)),]
# Classification Tree with rpart
library(rpart)

# Grow tree to predict whether a person took an offer 'yes' or not 'no'
fit = rpart(class ~ ., method='class', data=wine.data)
printcp(fit) # display the results
plotcp(fit) # visualize cross-validation results
summary(fit) # detailed summary of splits

# plot tree
plot(fit, uniform=TRUE,
     main="Classification Tree for wine quality")
text(fit, use.n=TRUE, all=TRUE, cex=.7)
post(fit, file=file.choose(), title="Classification Tree for wine quality")

# prune the tree
pfit = prune(fit, cp=fit$cptable[which.min(fit$cptable[,'xerror']), 'CP'])
# plot pruned tree
plot(pfit, uniform=TRUE,
     main="Pruned Classification Tree for wine quality")
text(fit, use.n=TRUE, all=TRUE, cex=.7)

tree.pred.classes <- predict(pfit, data.tst, type='vector')

# on average how many samples have been properly predicted
mean(tree.pred.classes == true.classes)
