# Projekt - MOW

# Tworzenie pakietu
  [Tutorial](https://tinyheero.github.io/jekyll/update/2015/07/26/making-your-first-R-package.html)
  1. Setup
    install.packages("devtools")
    install.packages("roxygen2")
  2. Create package
    devtools::create("name")
  3. Generate documentation
      devtools::document()
  4. Add data to package
      x <- data
      usethis::use_data(x)
      

    
  
  